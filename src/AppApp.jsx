import React from "react";
import LearnUseState2 from "./component/2LearnUseState";
import AxiousPractice from "./component/AxiousPractice";
import Check from "./component/Check";
import LearnForm from "./component/Form";
import HomeWork from "./component/HomeWork";
import FormikForm from "./component/LearnFormik/FormikForm";
import FormikTutorial from "./component/LearnFormik/FormikTutorial";
import AddProduct from "./component/LearnRouting/AddProduct";
import LearnRout from "./component/LearnRouting/LearnRout";
import LearnToHitApi from "./component/LearnToHitApi";
import UseEffectLearn from "./component/LearnUseEffect/UseEffectLearn";
import LearnUseState from "./component/LerntUseState";
import Form1 from "./component/LForm/Form1";
import P1 from "./component/ParentChidlBehaviour/P1";
import LearnSetTimeOut from "./component/setTimeOutsetTimeInterval";
import UseRefPractice from "./component/UseRefPractice";
import { NavLink, Route, Routes } from "react-router-dom";
import GetProduct from "./component/LearnRouting/GetProduct";
import NestingRoute from "./component/LearnRouting/NestingRoute";
const AppApp = () => {
  return (
    <div>
      {/* <LearnUseState></LearnUseState> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <P1></P1> */}

      {/* <Check></Check> */}
      {/* <UseEffectLearn></UseEffectLearn> */}
      {/* <HomeWork></HomeWork> */}
      {/* <LearnForm></LearnForm> */}
      {/* <Form1></Form1> */}
      {/* <FormikForm></FormikForm> */}
      {/* <FormikTutorial></FormikTutorial> */}

      {/* <LearnSetTimeOut></LearnSetTimeOut> */}

      {/* <UseRefPractice></UseRefPractice> */}

      {/* <AxiousPractice></AxiousPractice> */}

      {/* <LearnToHitApi></LearnToHitApi> */}
      {/* <div>hello</div> */}

      {/* <LearnRout></LearnRout> */}

      {/* <div>hello</div> */}
      {/* <NestingRoute></NestingRoute> */}

      {/* <AddProduct></AddProduct> */}
      {/* <GetProduct></GetProduct> */}
    </div>
  );
};

export default AppApp;
// let obj = { name: "nitan", age: "28" };
