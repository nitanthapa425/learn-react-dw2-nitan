import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Routes, Outlet } from "react-router-dom";
import { changeCity, changeProvince } from "./feature/addressSlice";
import { changeBlogDescription, changeBlogTitle } from "./feature/blogSlice";
import { changeAge, changeName } from "./feature/counterSlice";
import { changeProductName } from "./feature/productSlice";
import CreateBlog from "./ProjectComponent/Blog/CreateBlog";
import ReadBlogs from "./ProjectComponent/Blog/ReadBlogs";
import ReadBlogsById from "./ProjectComponent/Blog/ReadBlogsById";
import UpdateBlog from "./ProjectComponent/Blog/UpdateBlog";
import CreateProduct from "./ProjectComponent/CreateProduct";
import CreateProductUsingRtk from "./ProjectComponent/CreateProductUsingRtk";
import Footer from "./ProjectComponent/Footer";
import NavBar from "./ProjectComponent/NavBar";
import ShowAllProducts from "./ProjectComponent/ShowAllProducts";
import ShowAllProductsUsingRtk from "./ProjectComponent/ShowAllProductsUsingRTK";
import UpdateProduct from "./ProjectComponent/UpdateProduct";
import UpdateProductUsingRtk from "./ProjectComponent/UpdateProductUsingRtk";
import ViewProduct from "./ProjectComponent/ViewProduct";
import ViewProductUsingRtk from "./ProjectComponent/ViewProductUsingRtk";

//localhost:3000
// localhost:3000/products   give all products
// localhost:3000/products/:id  give all products
// localhost:3000/products/create    //form to add product
// localhost:3000/products/update/:id    //form to add product

//creat producte
//products read
//product delete
//get single product

const Project = () => {
  let dispatch = useDispatch();
  let info = useSelector((store) => {
    return store.info;
  });

  let addressData = useSelector((store) => {
    return store.address;
  });
  let productData = useSelector((store) => {
    return store.product;
  });
  let blog = useSelector((store) => {
    return store.blog;
  });

  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <NavBar></NavBar> <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            {/* <Route index element={<ShowAllProducts></ShowAllProducts>}></Route> */}
            <Route
              index
              element={<ShowAllProductsUsingRtk></ShowAllProductsUsingRtk>}
            ></Route>
            {/* <Route path=":id" element={<ViewProduct></ViewProduct>}></Route> */}
            <Route
              path=":id"
              element={<ViewProductUsingRtk></ViewProductUsingRtk>}
            ></Route>
            {/* <Route
              path="create"
              element={<CreateProduct></CreateProduct>}
            ></Route> */}
            <Route
              path="create"
              element={<CreateProductUsingRtk></CreateProductUsingRtk>}
            ></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              {/* <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route> */}
              <Route
                path=":id"
                element={<UpdateProductUsingRtk></UpdateProductUsingRtk>}
              ></Route>
            </Route>
          </Route>
          <Route
            path="blogs"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadBlogs></ReadBlogs>}></Route>

            <Route path=":id" element={<ReadBlogsById></ReadBlogsById>}></Route>

            <Route path="create" element={<CreateBlog></CreateBlog>}></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route path=":id" element={<UpdateBlog></UpdateBlog>}></Route>
            </Route>
          </Route>
        </Route>
      </Routes>
      {/* <div>name={info.name}</div>
      <div>age={info.age}</div>
      <div>city : {addressData.city}</div>
      <div>province : {addressData.province}</div>
      <div>product Name : {productData.name}</div>
      <div>Blog Title: {blog.title}</div>
      <div>Blog Description: {blog.description}</div>
      <button
        onClick={() => {
          // dispatch(changeName());
          // dispatch(changeName("hari"));
          dispatch(changeName("hari"));
        }}
      >
        change Name
      </button>

      <button
        onClick={() => {
          dispatch(changeAge(30));
        }}
      >
        change Age
      </button>

      <button
        onClick={() => {
          dispatch(changeCity("bha"));
        }}
      >
        change city
      </button>
      <button
        onClick={() => {
          dispatch(changeProvince(4));
        }}
      >
        change province
      </button>
      <button
        onClick={() => {
          dispatch(changeProductName("Samsung"));
        }}
      >
        change Product Name
      </button>
      <button
        onClick={() => {
          dispatch(changeBlogTitle("Morning Blog"));
        }}
      >
        change Blog Title
      </button>
      <button
        onClick={() => {
          dispatch(changeBlogDescription("Morning Blog Description"));
        }}
      >
        change Blog Description
      </button> */}
    </div>
  );
};

export default Project;
