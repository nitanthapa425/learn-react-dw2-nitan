import { Form, Formik } from "formik";
import React from "react";

import FormikInput from "../../component/LearnFormik/FormikInput";
import FormikTextArea from "../../component/LearnFormik/FormikTextArea";
import { blogValidationSchema } from "../../validation/blogValidation";

const BlogForm = ({
  buttonName = "Create Blog",
  onSubmit = () => {},
  blog = {},
  isLoading = false,
}) => {
  let initialValues = {
    title: blog.title || "",
    description: blog.description || "",
    img: blog.img || "",
  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={blogValidationSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          //for formik we need
          // name
          // label
          // type
          // onChange
          // required
          //options ( radio, select)  (array of object)
          return (
            <Form>
              <FormikInput
                name="title"
                label="Title"
                type="text"
                required={true}
              ></FormikInput>
              <FormikInput
                name="img"
                label="Img"
                type="text"
                required={true}
              ></FormikInput>

              <FormikTextArea
                name="description"
                label="Description"
                type="text"
                required={true}
              ></FormikTextArea>

              <button type="submit">
                {isLoading ? (
                  <div>{buttonName}...</div>
                ) : (
                  <div>{buttonName}</div>
                )}
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default BlogForm;
