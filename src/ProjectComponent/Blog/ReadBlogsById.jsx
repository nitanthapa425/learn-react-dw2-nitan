import React, { useEffect } from "react";
import { useParams } from "react-router";
import { useReadBlogByIdQuery } from "../../services/api/blogService";
// import { useReadBlogByIdQuery } from "../services/api/blogService";

const ViewBlogUsingRtk = () => {
  let params = useParams();
  let id = params.id;

  let {
    isLoading: isLoadingReadDetails,
    isError: isErrorReadDetails,
    error: errorReadDetails,
    data: dataReadDetails,
  } = useReadBlogByIdQuery(id);

  let blog = dataReadDetails?.data || {};

  useEffect(() => {
    if (isErrorReadDetails) {
      console.log(errorReadDetails.error);
    }
  }, [isErrorReadDetails, errorReadDetails]);

  return (
    <div>
      {isLoadingReadDetails ? (
        <div>
          <h1>Loading ...</h1>
        </div>
      ) : (
        <div>
          <img
            alt="blog img"
            src={blog.img}
            style={{ width: "100px", height: "100px" }}
          ></img>
          <p>title : {blog.title}</p>
          <p>Description : {blog.description}</p>
        </div>
      )}
    </div>
  );
};

export default ViewBlogUsingRtk;
