import React, { useEffect } from "react";

import { useParams, useNavigate } from "react-router";
import {
  useReadBlogByIdQuery,
  useUpdatedBlogMutation,
} from "../../services/api/blogService";
import BlogForm from "./BlogForm";

const UpdateBlogUsingRtk = () => {
  let navigate = useNavigate();
  let params = useParams();
  let id = params.id;

  let [
    updateBlog,
    {
      isLoading: isLoadingUpdateData,
      isSuccess: isSuccessUpdateData,
      isError: isErrorUpdateData,
      error: errorUpdateData,
    },
  ] = useUpdatedBlogMutation();

  let {
    isError: isErrorReadByIdData,
    error: errorReadByIdData,
    data: dataReadByIdData,
  } = useReadBlogByIdQuery(id);

  let blog = dataReadByIdData?.data || {};

  // console.log("***************", blog.data);

  useEffect(() => {
    if (isSuccessUpdateData) {
      console.log("Successfully updated");
      navigate(`/blogs/${id}`);
    }
  }, [isSuccessUpdateData]);
  useEffect(() => {
    if (isErrorUpdateData) {
      console.log(errorUpdateData.error);
    }
  }, [isErrorUpdateData, errorUpdateData]);
  useEffect(() => {
    if (isErrorReadByIdData) {
      console.log(errorReadByIdData.error);
    }
  }, [isErrorReadByIdData, errorReadByIdData]);

  let onSubmit = async (values, other) => {
    updateBlog({ id: id, body: values });
  };

  return (
    <div>
      <BlogForm
        buttonName="Update Blog"
        onSubmit={onSubmit}
        blog={blog}
        isLoading={isLoadingUpdateData}
      ></BlogForm>
    </div>
  );
};

export default UpdateBlogUsingRtk;
