import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikCheckBox from "../component/LearnFormik/FormikCheckBox";
import FormikInput from "../component/LearnFormik/FormikInput";
import FormikSelect from "../component/LearnFormik/FormikSelect";
import htmlDateFormat from "../ProjectUtils/inputdateFormat";
import ProductForm from "./ProductForm";
// import ProjectForm from "./ProductForm";
import { useNavigate } from "react-router";
import { hitApi } from "../services/hitapi";

//creat product form
//hit api onSubmit

const CreateProduct = () => {
  let navigate = useNavigate();

  let onSubmit = async (values, other) => {
    console.log(values);
    try {
      let output = await hitApi({
        method: "post",
        url: "/products",
        data: values,
      });
      navigate("/products");
    } catch (error) {
      console.log("****", error.message);
    }
  };

  return (
    <div>
      <ProductForm
        buttonName="Create Product"
        onSubmit={onSubmit}
      ></ProductForm>
    </div>
  );
};

export default CreateProduct;
