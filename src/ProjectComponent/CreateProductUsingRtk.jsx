import React, { useEffect } from "react";

import ProductForm from "./ProductForm";

import { useNavigate } from "react-router";
import { useCreateProductMutation } from "../services/api/productService";

const CreateProductUsingRtk = () => {
  let navigate = useNavigate();
  let [
    createProduct,
    {
      isLoading: isLoadingCreateData,
      isSuccess: isSuccessCreateData,
      isError: isErrorCreateData,
      error: errorCreateData,
    },
  ] = useCreateProductMutation();

  useEffect(() => {
    if (isSuccessCreateData) {
      console.log("Successfully created");
      navigate("/products");
    }
  }, [isSuccessCreateData]);
  useEffect(() => {
    if (isErrorCreateData) {
      console.log(errorCreateData.error);
    }
  }, [isErrorCreateData, errorCreateData]);

  let onSubmit = async (values, other) => {
    let body = values;

    createProduct(body);
  };

  return (
    <div>
      <ProductForm
        buttonName="Create Product"
        onSubmit={onSubmit}
        isLoading={isLoadingCreateData}
      ></ProductForm>
    </div>
  );
};

export default CreateProductUsingRtk;
