import React from "react";
import { NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <div>
      <nav style={{ backgroundColor: "red" }}>
        <NavLink to="/products" style={{ marginLeft: "20px", color: "white" }}>
          Products
        </NavLink>
        <NavLink
          to="/products/create"
          style={{ marginLeft: "20px", color: "white" }}
        >
          Create Product
        </NavLink>
        <NavLink to="/blogs" style={{ marginLeft: "20px", color: "white" }}>
          Blogs
        </NavLink>
        <NavLink
          to="/blogs/create"
          style={{ marginLeft: "20px", color: "white" }}
        >
          Create Blog
        </NavLink>
      </nav>
    </div>
  );
};

export default NavBar;
