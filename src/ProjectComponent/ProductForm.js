import { Form, Formik } from "formik";
import React from "react";

import FormikCheckBox from "../component/LearnFormik/FormikCheckBox";
import FormikInput from "../component/LearnFormik/FormikInput";
import FormikSelect from "../component/LearnFormik/FormikSelect";
import { companyOptions } from "../config/config";
import htmlDateFormat from "../ProjectUtils/inputdateFormat";
import { productValidationSchema } from "../validation/productValidation";

//creat product form
//hit api onSubmit

const ProductForm = ({
  buttonName = "Create Product",
  onSubmit = () => {},
  product = {},
  isLoading = false,
}) => {
  // let initialValues = {
  //   name:"" ,
  //   quantity: 0,
  //   price: 0,
  //   featured: false,
  //   productImage: "",
  //   manufactureDate: htmlDateFormat(new Date()),
  //   company: "apple",
  // };

  let initialValues = {
    name: product.name || "",
    quantity: product.quantity || 1,
    price: product.price || 100,
    featured: product.featured || false,
    productImage: product.productImage || "",
    manufactureDate: htmlDateFormat(product.manufactureDate || new Date()),
    company: product.company || "apple",
  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={productValidationSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          //for formik we need
          // name
          // label
          // type
          // onChange
          // required
          //options ( radio, select)  (array of object)
          return (
            <Form>
              <FormikInput
                name="name"
                label="Name"
                type="text"
                required={true}
              ></FormikInput>
              <FormikInput
                name="quantity"
                label="Quantity"
                type="number"
                required={true}
              ></FormikInput>
              <FormikInput
                name="price"
                label="Price"
                type="number"
                required={true}
              ></FormikInput>

              <FormikCheckBox name="featured" label="Featured"></FormikCheckBox>
              <FormikInput
                name="productImage"
                label="Product Image"
                type="text"
                required={true}
              ></FormikInput>
              <FormikInput
                name="manufactureDate"
                label="Manufacture Date"
                type="date"
                required={true}
              ></FormikInput>

              <FormikSelect
                name="company"
                label="Company"
                required={true}
                options={companyOptions}
              ></FormikSelect>

              <button type="submit">
                {isLoading ? (
                  <div>{buttonName}...</div>
                ) : (
                  <div>{buttonName}</div>
                )}
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default ProductForm;
