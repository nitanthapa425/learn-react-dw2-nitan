import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { hitApi } from "../services/hitapi";
// onclick function always takes function ; we should not use function call if we use function call we must return a function
// use function that return function when we need to pass a value

// (api) => (data) => display;

//delete product

const ShowAllProducts = () => {
  let [products, setProducts] = useState([]);

  let navigate = useNavigate();

  let getProduct = async () => {
    try {
      let output = await hitApi({
        method: "get",
        url: "/products",
      });

      setProducts(output.data.data.results);
    } catch (error) {
      console.log(error.message);
    }

    // console.log(output);
  };

  //we can not use asyc keyword to the useEffect function
  useEffect(() => {
    getProduct();
  }, []);

  let handleView = (item) => {
    return () => {
      navigate(`/products/${item._id}`);
    };
  };
  let handleclick = () => {
    console.log("button is clicked");
  };
  let handleEdit = (item) => {
    return () => {
      navigate(`/products/update/${item._id}`);
    };
  };

  return (
    <div>
      {products.map((item, i) => {
        return (
          <div style={{ border: "solid red 3px", marginBottom: "5px" }}>
            <img
              alt="product img"
              src={item.productImage}
              style={{ width: "80px", height: "80px" }}
            ></img>
            <br></br>
            name: {item.name}
            <br></br>
            Price: {item.price}
            <br></br>
            Quantity: {item.quantity}
            <br></br>
            company: {item.company}
            <br></br>
            <button
              onClick={async (e) => {
                try {
                  await hitApi({
                    method: "delete",
                    url: `/products/${item._id}`,
                  });

                  getProduct();
                } catch (error) {
                  console.log(error);
                }
              }}
            >
              Delete Product
            </button>
            <button onClick={handleView(item)}>View Product</button>
            {/* onClick need function */}
            {/* we can use function in onClick
            or we can use function call in onClick but this time it must return function
            a functionCall way will be used if you want to pass some data
            if you dont want to pass data then use function defination
             */}
            <button onClick={handleEdit(item)}>Edit Product</button>
            <button onClick={handleclick}>click me</button>
          </div>
        );
      })}
    </div>
  );
};

export default ShowAllProducts;
