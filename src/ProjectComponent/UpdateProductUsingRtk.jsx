import React, { useEffect } from "react";

import { useParams, useNavigate } from "react-router";

import ProductForm from "./ProductForm";

import {
  useReadProductByIdQuery,
  useUpdatedProductMutation,
} from "../services/api/productService";

const UpdateProductUsingRtk = () => {
  let navigate = useNavigate();
  let params = useParams();
  let id = params.id;

  let [
    updateProduct,
    {
      isLoading: isLoadingUpdateData,
      isSuccess: isSuccessUpdateData,
      isError: isErrorUpdateData,
      error: errorUpdateData,
    },
  ] = useUpdatedProductMutation();

  let {
    isError: isErrorReadByIdData,
    error: errorReadByIdData,
    data: dataReadByIdData,
  } = useReadProductByIdQuery(id);

  let product = dataReadByIdData?.data || {};

  // console.log("***************", product.data);

  useEffect(() => {
    if (isSuccessUpdateData) {
      console.log("Successfully updated");
      navigate(`/products/${id}`);
    }
  }, [isSuccessUpdateData]);
  useEffect(() => {
    if (isErrorUpdateData) {
      console.log(errorUpdateData.error);
    }
  }, [isErrorUpdateData, errorUpdateData]);
  useEffect(() => {
    if (isErrorReadByIdData) {
      console.log(errorReadByIdData.error);
    }
  }, [isErrorReadByIdData, errorReadByIdData]);

  let onSubmit = async (values, other) => {
    updateProduct({ id: id, body: values });
  };

  return (
    <div>
      <ProductForm
        buttonName="Update Product"
        onSubmit={onSubmit}
        product={product}
        isLoading={isLoadingUpdateData}
      ></ProductForm>
    </div>
  );
};

export default UpdateProductUsingRtk;
