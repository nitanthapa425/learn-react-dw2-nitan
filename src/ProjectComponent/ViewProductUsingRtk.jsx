import React, { useEffect } from "react";
import { useParams } from "react-router";
import { useReadProductByIdQuery } from "../services/api/productService";

const ViewProductUsingRtk = () => {
  let params = useParams();
  let id = params.id;

  let {
    isLoading: isLoadingReadDetails,
    isError: isErrorReadDetails,
    error: errorReadDetails,
    data: dataReadDetails,
  } = useReadProductByIdQuery(id);

  let product = dataReadDetails?.data || {};

  useEffect(() => {
    if (isErrorReadDetails) {
      console.log(errorReadDetails.error);
    }
  }, [isErrorReadDetails, errorReadDetails]);

  return (
    <div>
      {isLoadingReadDetails ? (
        <div>
          <h1>Loading ...</h1>
        </div>
      ) : (
        <div>
          <img
            alt="product img"
            src={product.productImage}
            style={{ width: "100px", height: "100px" }}
          ></img>
          <p>product name : {product.name}</p>
          <p>product company : {product.company}</p>
          <p> is in featured : {product.featured ? "yes" : "no"}</p>

          <p>
            {" "}
            manufactureDate :
            {new Date(product.manufactureDate).toLocaleDateString()}
          </p>

          <p>price: NRs. {product.price}</p>
          <p>quantity : {product.quantity}</p>
        </div>
      )}
    </div>
  );
};

export default ViewProductUsingRtk;
