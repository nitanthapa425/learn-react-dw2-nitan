import React, { useState } from "react";

const LearnUseState2 = () => {
  let [info, setInfo] = useState({
    name: "nitan",
    age: 28,
    address: "gagalphedi",
  });
  return (
    <div>
      name is {info.name}
      age is {info.age}
      address is {info.address}
      <button
        onClick={() => {
          setInfo({
            ...info,
            name: "sam",
          });
        }}
      >
        change name
      </button>
      <button
        onClick={() => {
          setInfo({
            ...info,
            address: "kathmandu",
          });
        }}
      >
        change address
      </button>
      <button
        onClick={() => {
          setInfo({
            ...info,
            age: 28,
          });
        }}
      >
        change age
      </button>
    </div>
  );
};

export default LearnUseState2;
