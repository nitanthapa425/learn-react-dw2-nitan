import React from "react";
import { hitApi } from "../services/hitapi";

const AxiousPractice = () => {
  let createProduct = async () => {
    // console.log("i will create product");

    try {
      let result = await hitApi({
        method: "post",
        url: "/products",
        data: {
          name: "Product2",
          quantity: 13,
          price: 12,
          featured: true,
          productImage:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1280px-Image_created_with_a_mobile_phone.png",
          manufactureDate: "2022-1-24",
          company: "apple",
        },
      });

      console.log(result);
    } catch (error) {
      console.log(error.message);
    }
  };

  let readProduct = async () => {
    // console.log("i will read product");
    try {
      let result = await hitApi({
        method: "get",
        url: "/products",
      });
      console.log(result);
    } catch (error) {
      console.log(error.message);
    }
  };
  let readById = async () => {
    // console.log("i will read product");
    try {
      let result = await hitApi({
        method: "get",
        url: "/products/63ef36b4cafb7c9aa2662848",
      });
      console.log(result);
    } catch (error) {
      console.log(error.message);
    }
  };

  let updateProduct = async () => {
    try {
      let result = await hitApi({
        method: "patch",
        url: "/products/63ef32e5cafb7c9aa2662840",
        data: {
          name: "Product1",
          quantity: 13,
          price: 12,
          featured: true,
          productImage:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1280px-Image_created_with_a_mobile_phone.png",
          manufactureDate: "2022-1-24",
          company: "apple",
        },
      });

      console.log(result);
    } catch (error) {
      console.log(error.message);
    }
  };

  let deleteProduct = async () => {
    // 63ef36b4cafb7c9aa2662848
    try {
      let result = await hitApi({
        method: "delete",
        url: "/products/63ef36b4cafb7c9aa2662848",
      });

      console.log(result);
    } catch (error) {
      console.log(error.message);
    }
    console.log("i will delete product");
  };

  return (
    <div>
      AxiousPractice
      <br></br>
      <button onClick={createProduct}>Create Product</button>
      <br></br>
      <button onClick={readProduct}>Read Product</button>
      <br></br>
      <button onClick={readById}>Read By Product</button>
      <br></br>
      <button onClick={updateProduct}>Update Product</button>
      <br></br>
      <button onClick={deleteProduct}>Delete Product</button>
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1280px-Image_created_with_a_mobile_phone.png"></img>
    </div>
  );
};

export default AxiousPractice;
