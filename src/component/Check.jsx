import React from "react";
import { matches } from "../data";

const Check = () => {
  let players = (match) => {
    let _players = match.map((value, i) => {
      let ary = [value.team1, value.team2];

      return ary;
    });

    return _players;
  };

  // unique total players
  let totalPlayers = players(matches);
  let uniquePlayers = totalPlayers.reduce((pre, current) => {
    let result = [...new Set([...pre, ...current])];
    return result;
  }, []);

  // console.log(uniquePlayers);

  //game played by each
  let playedGame = matches.filter((value, i) => {
    if (value?.score?.ft?.length) {
      return true;
    }
  });

  let playedPlayer = players(playedGame);

  let playedPlayerInArray = playedPlayer.reduce((pre, current) => {
    let result = [...pre, ...current];
    return result;
  }, []);
  // console.log(playedPlayerInArray);

  //calculating the playes with total game
  let calculateWithRepeatingNumber = (array) => {
    let _calculateWithRepeatingNumber = array.reduce((pre, current) => {
      if (pre[current]) {
        let result = { ...pre, [current]: pre[current] + 1 };
        return result;
      } else {
        let result = { ...pre, [current]: 1 };
        return result;
      }
    }, {});
    return _calculateWithRepeatingNumber;
  };

  let clubWithPlayedGame = calculateWithRepeatingNumber(playedPlayerInArray);

  // console.log("clubWithPlayedGame", clubWithPlayedGame);

  //calculating the player with total win
  let calculateWithWinNumber = (array) => {
    let _calculateWithWinNumber = array.reduce((pre, current) => {
      if (current.score.ft[0] > current.score.ft[1]) {
        if (pre[current.team1] !== undefined) {
          let result = { ...pre, [current.team1]: pre[current.team1] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team1]: 1 };
          return result;
        }
      } else if (current.score.ft[1] > current.score.ft[0]) {
        if (pre[current.team2] !== undefined) {
          let result = { ...pre, [current.team2]: pre[current.team2] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team2]: 1 };
          return result;
        }
      }
      return pre;
    }, {});

    return _calculateWithWinNumber;
  };

  let winGame = calculateWithWinNumber(playedGame);

  // console.log(winGame);

  //calculating with loss game
  let calculateWithLossNumber = (array) => {
    let _calculateWithLossNumber = array.reduce((pre, current) => {
      if (current.score.ft[0] > current.score.ft[1]) {
        if (pre[current.team2] !== undefined) {
          let result = { ...pre, [current.team2]: pre[current.team2] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team2]: 1 };
          return result;
        }
      } else if (current.score.ft[1] > current.score.ft[0]) {
        if (pre[current.team1] !== undefined) {
          let result = { ...pre, [current.team1]: pre[current.team1] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team1]: 1 };
          return result;
        }
      }
      return pre;
    }, {});

    return _calculateWithLossNumber;
  };

  let lossGame = calculateWithLossNumber(playedGame);

  // console.log(lossGame);

  //calculating draw game
  let calculateWithDrawNumber = (array) => {
    let _calculateWithDrawNumber = array.reduce((pre, current) => {
      if (current.score.ft[0] === current.score.ft[1]) {
        if (
          pre[current.team1] !== undefined &&
          pre[current.team2] !== undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: pre[current.team1] + 1,
            [current.team2]: pre[current.team2] + 1,
          };
          return result;
        } else if (
          pre[current.team1] !== undefined &&
          pre[current.team2] === undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: pre[current.team1] + 1,
            [current.team2]: 1,
          };
          return result;
        } else if (
          pre[current.team2] !== undefined &&
          pre[current.team1] === undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: 1,
            [current.team2]: pre[current.team2] + 1,
          };
          return result;
        } else if (
          pre[current.team1] === undefined &&
          pre[current.team2] === undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: 1,
            [current.team2]: 1,
          };
          return result;
        }
      }
      return pre;
    }, {});

    return _calculateWithDrawNumber;
  };

  let drawGame = calculateWithDrawNumber(playedGame);

  // console.log(drawGame);

  //goal score and goal conceed
  let allGoalInfo = playedGame.reduce((previous, current) => {
    let teamAGoalInfo = {
      club: current.team1,
      goalScore: current.score.ft[0],
      goalConceed: current.score.ft[1],
      goalDifference: current.score.ft[0] - current.score.ft[1],
      date: current.date,
    };
    let teamBGoalInfo = {
      club: current.team2,
      goalScore: current.score.ft[1],
      goalConceed: current.score.ft[0],
      goalDifference: current.score.ft[1] - current.score.ft[0],
      date: current.date,
    };
    let returnValue = [...previous, teamAGoalInfo, teamBGoalInfo];

    return returnValue;
  }, []);

  let uniqueGoalInfo = allGoalInfo.reduce((previous, current) => {
    //if teamA repead itslef
    //perform add operation of goalScorea and goalConceed

    let returnValue = [];

    let alreadyExistTeam = previous.find((item, i) => {
      return item.club === current.club;
    });

    if (alreadyExistTeam) {
      let calculatedTeamScore = {
        club: current.club,
        goalScore: current.goalScore + alreadyExistTeam.goalScore,
        goalConceed: current.goalConceed + alreadyExistTeam.goalConceed,
        goalDifference:
          current.goalDifference + alreadyExistTeam.goalDifference,
      };

      let filteredPrevious = [...previous].filter((_item, _i) => {
        return _item.club !== current.club;
      });

      returnValue = [...filteredPrevious, calculatedTeamScore];
    }

    //else return current
    else {
      returnValue = [...previous, current];
    }

    return returnValue;
  }, []);

  // console.log(uniqueGoalInfo["Brighton & Hove Albion FC"]);

  // console.log("********", allGoalInfo);

  // status of last 5 game

  let club5GameInfo = uniquePlayers.reduce((previous, current) => {
    let clubData = allGoalInfo.filter((_item, _i) => {
      // console.log(current.club);
      return _item.club === current;
    });

    let sortData = clubData.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });

    let latestFiveData = sortData.filter((value, i) => {
      if (i === 0 || i === 1 || i === 2 || i === 3 || i === 4) return value;
    });

    let winLossData = latestFiveData.reduce((_previous, _current) => {
      if (_current.goalDifference === 0) {
        return [..._previous, "D"];
      } else if (_current.goalDifference > 0) {
        return [..._previous, "W"];
      } else if (_current.goalDifference < 0) {
        return [..._previous, "L"];
      }
    }, []);

    let returnedData = [...previous, { club: current, status: winLossData }];
    return returnedData;
  }, []);

  // console.log("club", club5GameInfo);

  //whole tabel

  let newTable = uniquePlayers.map((club, i) => {
    let obj = { club: club };
    if (clubWithPlayedGame[club] !== undefined) {
      obj = { ...obj, clubWithPlayedGame: clubWithPlayedGame[club] };
    }

    if (winGame[club] !== undefined) {
      obj = { ...obj, winGame: winGame[club] };
    }

    if (lossGame[club] !== undefined) {
      obj = { ...obj, lossGame: lossGame[club] };
    }

    if (drawGame[club] !== undefined) {
      obj = { ...obj, drawGame: drawGame[club] };
    }
    let clubWithGoalInfo = uniqueGoalInfo.find((item, i) => {
      return item.club === club;
    });

    if (clubWithGoalInfo) {
      obj = { ...obj, ...clubWithGoalInfo };
    }

    let last5GoalInfo = club5GameInfo.find((item, i) => {
      return item.club === club;
    });

    if (last5GoalInfo) {
      obj = { ...obj, ...last5GoalInfo };
    }

    return obj;
  });

  let addPoint = newTable.map((item, i) => {
    let point = (item.drawGame || 0) + (item.winGame || 0) * 3;
    let newItem = { ...item, point };
    return newItem;
  });

  console.log("withpoint", addPoint);

  return <div>Check</div>;
};

export default Check;
