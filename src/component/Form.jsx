import React, { useState } from "react";

const LearnForm = () => {
  let [name, setName] = useState("");
  let [description, setDescription] = useState("");
  let [country, setCountry] = useState("");
  let [gender, setGender] = useState("");
  let [isMarried, setIsMarried] = useState(false);

  let handleSubmit = (e) => {
    e.preventDefault();

    console.log("name", name);
    console.log("description", description);
    console.log("country", country);
    console.log("gender", gender);
    console.log("isMarried", isMarried);

    // console.log("form is submitted successfully");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="nameid"> Name</label>
        <input
          id="nameid"
          placeholder="name"
          type="text"
          value={name} //a
          onChange={(e) => {
            // e.target.value;
            setName(e.target.value);
          }}
        ></input>

        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>

        <label htmlFor="descriptionid"> Description</label>
        <textarea
          id="descriptionid"
          placeholder="Description"
          value={description} //a
          onChange={(e) => {
            // e.target.value;
            setDescription(e.target.value);
          }}
        ></textarea>

        <br></br>
        <br></br>
        <br></br>

        <label htmlFor="country">Select country</label>
        <select
          value={country}
          onChange={(e) => {
            setCountry(e.target.value);
          }}
          id="country"
        >
          <option value="">Select Country</option>
          <option value="nepal">Nepal</option>
          <option value="india">India</option>
          <option value="china">China</option>
          <option value="america">America</option>
        </select>

        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <div>
          <label htmlFor="male">Male</label>
          <input
            type="radio"
            checked={gender === "male"}
            value="male"
            onChange={(e) => {
              setGender(e.target.value);
            }}
            id="male"
          ></input>
          <label htmlFor="female">Female</label>
          <input
            type="radio"
            checked={gender === "female"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
            value="female"
            id="female"
          ></input>
          <label htmlFor="other">Other</label>
          <input
            id="other"
            type="radio"
            checked={gender === "other"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
            value="other"
          ></input>

          <br></br>
          <br></br>
          <br></br>

          <label htmlFor="isMarried">Is Married</label>
          <input
            type="checkbox"
            checked={isMarried === true}
            id={"isMarried"}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          ></input>
        </div>

        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default LearnForm;

// all input has value and onChange property
//but the input whose name includes boxes eg radioboxes, checkbox has checked and onChange property

//those input which has options has one extra property named value (which is used in e.target.value)

//all has e.target.value except for checkbox it has e.target.checked
