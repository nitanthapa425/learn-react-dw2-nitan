import React, { useState } from "react";

const HomeWork = () => {
  let [showError, setShowError] = useState(false);

  return (
    <div>
      {showError ? <div>Error has occured</div> : null}

      <button
        onClick={() => {
          setShowError(true);
        }}
      >
        Show Error
      </button>
      <br></br>
      <button
        onClick={() => {
          setShowError(false);
        }}
      >
        Hide Error
      </button>
    </div>
  );
};

export default HomeWork;
