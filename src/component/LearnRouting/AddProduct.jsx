import { Form, Formik } from "formik";
import React from "react";
import { NavLink, Route, Routes, Outlet } from "react-router-dom";
import * as yup from "yup";
import { hitApi } from "../../services/hitapi";
import FormikCheckBox from "../LearnFormik/FormikCheckBox";
import FormikInput from "../LearnFormik/FormikInput";
import FormikSelect from "../LearnFormik/FormikSelect";
// post
// https://project-dw.onrender.com/api/v1/products
//  data: {
//           name: "Product2",
//           quantity: 13,
//           price: 12,
//           featured: true,
//           productImage:
//             "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1280px-Image_created_with_a_mobile_phone.png",
//           manufactureDate: "2022-1-24",
//           company: "apple",
//         },

const AddProduct = () => {
  let initialValues = {
    name: "",
    quantity: 0,
    price: 0,
    featured: false,
    productImage: "",
    manufactureDate: "",
    company: "apple",
  };
  let onSubmit = async (value, other) => {
    console.log(value);

    let data = await hitApi({
      method: "post",
      url: "/products",
      data: value,
    });
  };
  let validationSchema = yup.object({
    // "nitan thapa"
    // "kamal khadaka"

    //pattern of fullName  (alphabet and space)

    //regex => it define patteren

    name: yup.string().required(" Name is Required."),
    quantity: yup.number().required(" Quantity is Required."),
    price: yup.number().required(" Quantity is Required."),
    featured: yup.boolean(),
    productImage: yup.string().required(" Product Image is Required."),
    manufactureDate: yup.string().required(" manufacutrDate is Required."),
    company: yup.string().required(" Company is Required."),
  });

  let companyOptions = [
    {
      label: "Select Company",
      value: "",
      disabled: true,
    },

    {
      label: "Apple",
      value: "apple",
    },
    {
      label: "Samsung",
      value: "samsung",
    },
    {
      label: "Dell",
      value: "dell",
    },
    {
      label: "Other",
      value: "mi",
    },
  ];
  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          //for formik we need
          // name
          // label
          // type  FormiInput
          // onChange
          // required
          //option ( radio, select)  (array of object)
          return (
            <Form>
              <FormikInput
                name="name"
                label="Name"
                type="text"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("name", e.target.value);
                }}
              ></FormikInput>

              <FormikInput
                name="quantity"
                label="Quantity"
                type="number"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("quantity", e.target.value);
                }}
              ></FormikInput>
              <FormikInput
                name="price"
                label="Price"
                type="number"
                required={true}
                onChange={(e) => {
                  formik.setFieldValue("price", e.target.value);
                }}
              ></FormikInput>

              <FormikCheckBox
                name="featured"
                label="Add product on Feature section"
                onChange={(e) => {
                  formik.setFieldValue("featured", e.target.checked);
                }}
              ></FormikCheckBox>
              <FormikInput
                name="productImage"
                label="Product Image"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("productImage", e.target.value);
                }}
              ></FormikInput>
              <FormikInput
                name="manufactureDate"
                label="manufactureDate"
                type="date"
                onChange={(e) => {
                  formik.setFieldValue("manufactureDate", e.target.value);
                }}
              ></FormikInput>

              <FormikSelect
                name="company"
                label="Company"
                onChange={(e) => {
                  formik.setFieldValue("company", e.target.value);
                }}
                options={companyOptions}
              ></FormikSelect>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default AddProduct;
