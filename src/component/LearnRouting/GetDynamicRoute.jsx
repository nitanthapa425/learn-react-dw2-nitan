import React from "react";
import { useParams } from "react-router";
// localhost:3000/b/:id1/id/:id2
const GetDynamicRoute = () => {
  const params = useParams();
  return (
    <div>
      GetDynamicRoute
      <br></br>
      {params.id1}
      <br></br>
      {params.id2}
    </div>
  );
};

export default GetDynamicRoute;
