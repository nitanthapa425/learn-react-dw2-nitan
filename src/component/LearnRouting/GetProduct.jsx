import React, { useEffect, useState } from "react";
import { hitApi } from "../../services/hitapi";

const GetProduct = () => {
  let [products, setProducts] = useState([]);

  let readProduct = async () => {
    let obj = {
      method: "get",
      url: "/products",
    };

    let output = await hitApi(obj);

    setProducts(output.data.data.results);
  };

  useEffect(() => {
    readProduct();
  }, []);

  console.log(products);

  let deleteProduct = async (id) => {
    let obj = {
      method: "delete",
      url: `/products/${id}`,
    };
    let resutl = await hitApi(obj);
    readProduct();
  };

  return (
    <div>
      {products.map((item, i) => {
        return (
          <div style={{ border: "solid red 3px", marginBottom: "4px" }}>
            <img
              alt="product img "
              src={item.productImage}
              style={{ width: "100px", height: "100px" }}
            ></img>
            <br></br>
            <p>
              Product Name : <span>{item.name}</span>
              Product Price: <span>{item.price}</span>
              Product Quantity: <span>{item.quantity}</span>
            </p>
            <button
              onClick={() => {
                deleteProduct(item._id);
              }}
            >
              delete product
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default GetProduct;
