import React from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import GetDynamicRoute from "./GetDynamicRoute";
import About from "./LearnRoutFlow/About";
import Contact from "./LearnRoutFlow/Contact";
import Error from "./LearnRoutFlow/Error";
import Home from "./LearnRoutFlow/Home";
// / means our main url
//localhost:3000
//loclhost:3000/about
const LearnRout = () => {
  return (
    <div>
      <div>
        {/* change url(or route )  when link is clicked*/}

        {/* <div>
          <a href="/" style={{ marginLeft: "20px" }}>
            Home
          </a>
          <a href="/about" style={{ marginLeft: "20px" }}>
            About
          </a>
          <a href="/contact" style={{ marginLeft: "20px" }}>
            Contact
          </a>
          <a href="/other" style={{ marginLeft: "20px" }}>
            Other
          </a>
          <br></br>
          
        </div> */}
        {/* when we change route use a tag two things happen first it change path and reload whole page */}
        {/* but when we use NavLik it only change path (it does not reload page) */}
        <NavLink to="/" style={{ marginLeft: "20px" }}>
          Home
        </NavLink>
        <NavLink to="/about" style={{ marginLeft: "20px" }}>
          About
        </NavLink>
        <NavLink to="/contact" style={{ marginLeft: "20px" }}>
          Contact
        </NavLink>
        <NavLink to="/other" style={{ marginLeft: "20px" }}>
          Other
        </NavLink>

        {/* define component for different routes */}
        <Routes>
          <Route path="/" element={<Home></Home>}></Route>
          <Route path="/about" element={<About></About>}></Route>
          <Route path="/contact" element={<Contact></Contact>}></Route>
          {/* * means path is other than defined path */}
          <Route path="*" element={<Error></Error>}></Route>
          <Route path="/a" element={<div>a</div>}></Route>
          <Route path="/a/a1" element={<div>a a1</div>}></Route>
          <Route path="/a/a1/a2" element={<div>a a2</div>}></Route>
          <Route path="/a/:any" element={<div>a any</div>}></Route>
          {/* note /a/:any  here any is other than a1 */}

          {/* for dynamic route path */}

          <Route
            path="b/:id1/id/:id2"
            element={<GetDynamicRoute></GetDynamicRoute>}
          ></Route>
        </Routes>
      </div>
    </div>
  );
};

export default LearnRout;
