import React from "react";
import { useNavigate } from "react-router";
import { useSearchParams } from "react-router-dom";

const About = () => {
  let navigate = useNavigate();
  let [searchParams] = useSearchParams();

  return (
    <div>
      About page
      <br></br>
      <button
        onClick={() => {
          navigate("/contact");
          // navigate("/contact", { replace: true });
          // replace true is useful for login case
        }}
      >
        {" "}
        go to contact page
      </button>
      <br></br>
      <button
        onClick={() => {
          navigate(-1);
        }}
      >
        {" "}
        Go back
      </button>
      <br></br>
      {searchParams.get("name")}
      <br></br>
      {searchParams.get("age")}
    </div>
  );
};

export default About;
