import React from "react";
import { Navigate } from "react-router";

//for other than define path
// either we show error page
//or we redirect to home (or dashboard page)
const Error = () => {
  // return <div>Error 404 page</div>;

  return <Navigate to="/"></Navigate>;
};

export default Error;
