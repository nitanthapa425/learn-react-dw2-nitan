import React from "react";
import { NavLink, Route, Routes, Outlet } from "react-router-dom";

// http://localhost:3000/ show Home Page
// http://localhost:3000/student show Student Page
// http://localhost:3000/*  show 404 page

// http://localhost:3000/
// http://localhost:3000/student
// http://localhost:3000/student/1
// http://localhost:3000/student/kamal

const NestingRoute = () => {
  // give / to home
  // dot give / to other
  return (
    <div>
      {/*      /
        /product
        /abc */}
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <Outlet></Outlet> Hello Nitan
            </div>
          }
        >
          <Route path="*" element={<div>404 Page</div>}></Route>
          <Route index element={<div>Home Page</div>}></Route>
          <Route path="product" element={<div>Product Page</div>}></Route>
        </Route>
      </Routes>
      {/* <Routes>
        <Route
          path="/"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route
            path="student"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<div>Student Page</div>}></Route>
            <Route path="1" element={<div>1</div>}></Route>
            <Route path="kamal" element={<div>Kamal page</div>}></Route>
          </Route>
          <Route path="*" element={<div>404 Page</div>}></Route>
          <Route index element={<div>Home Page</div>}></Route>
        </Route>
      </Routes> */}
    </div>
  );
};

export default NestingRoute;
