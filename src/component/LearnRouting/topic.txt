

1)Basic url information
url, route, query, route parameters , query parameters(search parameters)
eg facebook.com/a/b/c?name=nitan&age=28  (path or url)
any thing before ? is called route
any thing after ? is called query
a,b,c, are called route parameters
name, age are called query parameters it is also called search parameter because it is use for searching purpose

1)Basic setup
a)install react-router-dom package
b)wrap the component in index.js(main file) with BrowserRouter (so that all the Component can use Routing Feature)

2)change route(url) when Link is clicked (using NavLink)
   a)difference between a and NavLink

3)Define component(page) for specific path
    a) path / with HomeComponent
    b) path /about with AboutComponent
    c) path /contact with ContactComponent
    d) path /* with page not Found
    a) with path /a
    b) with path /a/a1 (it has higher preference than /a/:any)
    c) with path /a/a1/a2
    d with path /a/:any here :any is called dynamic route parameter 
      show /a/:any will run if route is /a/a2 , /a/a3, .... but not run for /a/a1 because it is already defined

4)get dynamic route parameters using useParams() hook
     a) path b/:id1/id/:id2  (get any1 an get any2)


6)get query parameters
         a) by using useSearchParams() hook
         b) syntax [searchParams] = useSearchParams()
         c) get name and age by searchParams.get("name")  searchParams.get("age")
   

5) navigate (changing route)
   a) using NavLink
   b) onClick event using useNavigate() hook by (navigate("/contact"))
   c) navigation("/", { replace: true });  replace true means replace the history (so that it prevent the back)(example login)
       i) browser stack
       ii) give Login Page Info Once login we should not get login page because to login another account we must logout out
       iii) navigate(-1) for go back
       



     
           


7) Route with path * (if route does not match)
   a)can be used To show 404 page or
   b) or redirect to other page specially / by using <Navigate to="/"></Navigate>


8)Nested Route (Learn your self)

// http://localhost:3000/ show Home Page
// http://localhost:3000/student show Student Page
// http://localhost:3000/*  show 404 page





// http://localhost:3000/  
// http://localhost:3000/student
// http://localhost:3000/student/1
// http://localhost:3000/student/kamal







Rules
1)you can not use  react-route-dom hook in those file where route is defined
//our hooks are useParams, useSearchParams , useNavigate





Our Main Router component 
<BrowserRouter>   //use in main file so that all can use RoutingFeature
<NavLink> it has to property  {/* NavLink changes route without render or refreshing page */}
<Routes>  it is used to wrap all route
<Route>  it has path property (we define component for specific path)




