import axios from "axios";
import React from "react";

const LearnToHitApi = () => {
  let readProduct = async () => {
    let obj = {
      method: "get",
      url: "https://project-dw.onrender.com/api/v1/products",
    };

    let output = await axios(obj);

    console.log(output.data.data.result);
  };

  let creatProduct = async () => {
    let data = {
      name: "product6",
      quantity: 2,
      price: 2,
      featured: true,
      productImage:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1280px-Image_created_with_a_mobile_phone.png",
      manufactureDate: "2022-1-24",
      company: "apple",
    };

    let obj = {
      method: "post",
      url: "https://project-dw.onrender.com/api/v1/products",
      data: data,
    };

    let output = await axios(obj);
  };

  let updateProduct = async () => {
    let obj = {
      method: "patch",
      url: "https://project-dw.onrender.com/api/v1/products/63ef5f660819deb1c1b96019",
      data: {
        name: "Product66",
      },
    };

    let resutl = await axios(obj);
  };

  let deleteProduct = async () => {
    let obj = {
      method: "delete",
      url: "https://project-dw.onrender.com/api/v1/products/63ef5f660819deb1c1b96019",
    };

    let resutl = await axios(obj);
  };
  return (
    <div>
      LearnToHitApi
      <br></br>
      <button onClick={creatProduct}>Creat Product</button>
      <button onClick={readProduct}>Read Products</button>
      <button onClick={updateProduct}>Update Products</button>
      <button onClick={deleteProduct}>Delete Products</button>
    </div>
  );
};

export default LearnToHitApi;
