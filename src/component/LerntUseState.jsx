import React, { useState } from "react";

const LearnUseState = () => {
  let [count1, setCount1] = useState(0);

  let [count2, setCount2] = useState(10);

  console.log("count1", count1);

  console.log("count2", count2);

  return (
    <div>
      count1 = {count1}
      <br></br>
      count2= {count2}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        count1
      </button>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        count2
      </button>
      <br></br>
      <button
        onClick={() => {
          console.log("*************", count1);
          console.log("*************", count2);
        }}
      >
        Extra
      </button>
    </div>
  );
};

export default LearnUseState;
