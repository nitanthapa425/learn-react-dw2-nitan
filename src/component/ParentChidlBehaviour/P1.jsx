import React, { useState } from "react";
import PC1 from "./PC1";

const P1 = () => {
  let [count, setCount] = useState(0);

  console.log("i am parent");

  return (
    <div>
      <PC1></PC1>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default P1;
