import React from "react";

const StoreElement = ({ abc, a, b, c, d }) => {
  let userInfo = [
    {
      name: "nitan",
      age: 28,
      address: "gagalphedi",
    },
    {
      name: "utshab",
      age: 29,
      address: "sankhu",
    },
    {
      name: "sagar",
      age: 30,
      address: "gagal",
    },
  ];
  console.log(a);
  console.log(b);
  console.log(c);
  console.log(d);

  return (
    <div>
      {userInfo.map((value, i) => {
        return (
          <div className={abc}>
            {value.name} {value.age} {value.address}
          </div>
        );
      })}
    </div>
  );
};

export default StoreElement;
