import React, { useRef } from "react";

const UseRefPractice = () => {
  //define ref
  //attact ref
  let nameRef = useRef(); //define useRef
  let addressRef = useRef();
  let ageRef = useRef();

  //   styel={{backgroudColr:"red"}}
  return (
    <div>
      <input placeholder="Name" ref={nameRef}></input>
      <br></br>
      <input placeholder="Age" ref={ageRef}></input>
      <br></br>
      <input placeholder="Address" ref={addressRef}></input>
      <br></br>

      <button
        onClick={() => {
          nameRef.current.style.backgroundColor = "red";
        }}
      >
        Change Bg of Name
      </button>

      <button
        onClick={() => {
          nameRef.current.focus();
        }}
      >
        Focus Name Input
      </button>

      <button
        onClick={() => {
          addressRef.current.style.color = "red";
        }}
      >
        Change Address Color
      </button>

      <button
        onClick={() => {
          console.log(nameRef);
          //   console.log(nameRef.current.value);
          //   console.log(ageRef.current.value);
          //   console.log(addressRef.current.value);
        }}
      >
        getAllInputValue
      </button>
    </div>
  );
};

export default UseRefPractice;
