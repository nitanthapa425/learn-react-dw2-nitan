import React, { useState } from "react";

const LearnSetTimeOut = () => {
  //setTimeOut
  //setInterval

  console.log("i am first");

  setTimeout(() => {
    console.log("hello");
  }, 2000);

  console.log("i am ....");

  setTimeout(() => {
    console.log("hello 2");
  }, 0);

  //   setInterval(() => {
  //     console.log("i am set interval");
  //   }, 3000);

  return (
    <div>
      <div>Hello</div>

      <button
        onClick={() => {
          console.log("button is clicked");
        }}
      >
        click me
      </button>
    </div>
  );
};

export default LearnSetTimeOut;
