// creating store
import { configureStore } from "@reduxjs/toolkit";
import addressSlice from "../feature/addressSlice";
import blogSlice from "../feature/blogSlice";
import counterSlice from "../feature/counterSlice";
import productSlice from "../feature/productSlice";
import { blogApi } from "../services/api/blogService";
import { productApi } from "../services/api/productService";

export const store = configureStore({
  reducer: {
    info: counterSlice,
    address: addressSlice,
    product: productSlice,
    blog: blogSlice,

    [productApi.reducerPath]: productApi.reducer,
    [blogApi.reducerPath]: blogApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([productApi.middleware, blogApi.middleware]),
});
