let firtLetterCapital = (array) => {
  let newArray = array.map((value, i) => {
    let valueArray = value.split("");

    let newValueArray = valueArray.map((_value, _i) => {
      if (_i === 0) {
        return _value.toUpperCase();
      } else {
        return _value;
      }
    });

    let valueString = newValueArray.join("");

    return valueString;
  });

  return newArray;
};

// let _firtLetterCapital = firtLetterCapital(["my", "name", "is"]);

// console.log(_firtLetterCapital);

let eachWordCapital = (sentence) => {
  let array = sentence.split(" ");
  let newArray = firtLetterCapital(array);
  let newSentence = newArray.join(" ");
  return newSentence;
};

let check = eachWordCapital("my name is nitan");

console.log(check);
