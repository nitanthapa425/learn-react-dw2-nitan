// let array =[1,2,5,6]=>14

// 1+2+5+6 =14
// 0+1=1
// 1+2=3
// 3+5=8
// 8+6=14

// console.log(result);

let products = [
  {
    name: "p1",
    price: 1,
    quantity: 2,
  },
  {
    name: "p2",
    price: 2,
    quantity: 2,
  },
  {
    name: "p3",
    price: 1,
    quantity: 3,
  },
];

let result = products.reduce((previous, item) => {
  let sum = previous + item.price * item.quantity;
  // let sum = previous + current;
  return sum;
}, 0);

console.log(result);
