let fun1 = (a, b, ...c) => {
  //   console.log("a", a);
  //   console.log("b", b);
  //   console.log("c", c);
  //   [3,4,5,6]
};

fun1(1, 2, 3, 4, 5, 6);

let [name, ...other] = ["nitan", 25, false];

// console.log(name);
// console.log(other);

let { school, ...otherInfo } = {
  location: "gagalphedi",
  school: "bajra",
  college: "nec",
};

console.log(school);
console.log(otherInfo); //{location:"gagalphed",college:"nec"}
