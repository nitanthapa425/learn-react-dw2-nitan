//array // sort

//Capital letter are always small in order
//ascending sort
let list = ["a", "c", "b", "z", "e"];

let sortList = list.sort();

//for descending sort
let desSortList = sortList.reverse();
// console.log(desSortList);

// let list1 = [1, 9, 3];
let list1 = [9, 22];
let list1Sort = list1.sort();
console.log(list1Sort);
