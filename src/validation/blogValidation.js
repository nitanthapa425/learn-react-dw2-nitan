import * as yup from "yup";
export let blogValidationSchema = yup.object({
  title: yup.string().required("Title is Required."),
  description: yup.string().required("Description is Required."),
  img: yup.string().required("Img is Required."),
});
