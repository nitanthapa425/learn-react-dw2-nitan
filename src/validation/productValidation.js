import * as yup from "yup";
export let productValidationSchema = yup.object({
  name: yup.string().required("Name is Required."),
  quantity: yup.number().required("Quantity  is Required."),
  price: yup.number().required("Price  is Required."),
  featured: yup.boolean(),
  productImage: yup.string().required("ProductImage  is Required."),
  manufactureDate: yup.string().required("Manufacture date  is Required."),
  company: yup.string().required("Company  is Required."),
});
